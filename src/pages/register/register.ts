import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';


/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  @ViewChild('username') rname;
  @ViewChild('password') rpassword;
  constructor(private fire: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
  }

  alert(message: string)
  {
    {
      let alert = this.alertCtrl.create({
        title: 'Info',
        subTitle: message,
        buttons: ['OK']
      });
      alert.present();
    }  
  }

  registeruser()
  {
    this.fire.auth.createUserWithEmailAndPassword(this.rname.value,this.rpassword.value)
    .then( data => {
        this.alert('Register');
    })
    .catch( error => {
      this.alert(error.message);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

}
