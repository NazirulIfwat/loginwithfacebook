import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Facebook } from '../../../node_modules/@ionic-native/facebook';
import { AngularFireAuth } from '../../../node_modules/angularfire2/auth';

/**
 * Generated class for the FacebookPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-facebook',
  templateUrl: 'facebook.html',
})
export class FacebookPage {
  isLoggedIn:boolean = false;
  users: any;
  @ViewChild('username') rname;
  @ViewChild('password') rpassword;
  constructor(public navCtrl: NavController, public navParams: NavParams, private fb: Facebook, private fire: AngularFireAuth,
    private alertCtrl: AlertController) {
    fb.getLoginStatus()
    .then(res => {
      console.log(res.status);
      if(res.status === "connect") {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    })
    .catch(e => console.log(e));
  }

  /////////////////////Login/////////////////////////////////////////////////////
  login() {
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then(res => {
        if(res.status === "connected") {
          this.isLoggedIn = true;
          this.getUserDetail(res.authResponse.userID);
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log('Error logging into Facebook', e));
  }

  logout() {
    this.fb.logout()
      .then( res => this.isLoggedIn = false)
      .catch(e => console.log('Error logout from Facebook', e));
  }

  getUserDetail(userid) {
    this.fb.api("/"+userid+"/?fields=id,email,name,picture,gender",["public_profile"])
      .then(res => {
        console.log(res);
        this.users = res;
      })
      .catch(e => {
        console.log(e);
      });
  }

  alert(message: string)
  {
    {
      let alert = this.alertCtrl.create({
        title: 'Info',
        subTitle: message,
        buttons: ['OK']
      });
      alert.present();
    }  
  }

  registeruser()
  {
    this.fire.auth.createUserWithEmailAndPassword(this.rname.value,this.rpassword.value)
    .then( data => {
        this.alert('Register');
    })
    .catch( error => {
      this.alert(error.message);
    });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad FacebookPage');
  }

}
