import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Diagnostic } from '../../../node_modules/@ionic-native/diagnostic';
import { Cordova } from '../../../node_modules/@ionic-native/core';

/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  i : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private diagnostic: Diagnostic) {
    let successCallback = (isAvailable) => { console.log('Is available? ' + isAvailable); };
    let errorCallback = (e) => console.error(e);

    this.diagnostic.isCameraAvailable().then(successCallback).catch(errorCallback);
    this.diagnostic.isBluetoothAvailable().then(successCallback, errorCallback);
    this.diagnostic.isWifiAvailable().then(successCallback, errorCallback);
    this.diagnostic.isMotionAvailable().then(successCallback, errorCallback);
    this.diagnostic.isRemoteNotificationsEnabled().then(successCallback, errorCallback);
    this.diagnostic.isLocationAvailable().then(successCallback, errorCallback);
    this.diagnostic.isGpsLocationAvailable().then(successCallback, errorCallback);
    this.diagnostic.isNetworkLocationAvailable().then(successCallback, errorCallback);
    this.diagnostic.isNFCAvailable().then(successCallback, errorCallback);
    this.diagnostic.getExternalStorageAuthorizationStatus().then(successCallback, errorCallback);

  }

  BluetoothFunction(i)
  {
    switch (i)
    {
      case 1:
      
      this.diagnostic.getBluetoothState()
      .then((state) => {

        if (state == this.diagnostic.bluetoothState.POWERED_ON){ } 
        else if (state == this.diagnostic.bluetoothState.POWERED_OFF) { }
        else if (state == this.diagnostic.bluetoothState.UNAUTHORIZED) { }
        else if (state == this.diagnostic.bluetoothState.UNKNOWN) { }
        else if (state == this.diagnostic.bluetoothState.UNSUPPORTED) { }

      }).catch(e => console.error(e));
      break;

    case 2:

      this.diagnostic.isBluetoothEnabled()
      .then((response) => {
        console.log("Bluetooth is " + (response ? "enabled" : "disabled"));
      }).catch(e => console.error(e));
       break;

    case 3:
        this.diagnostic.setBluetoothState(true).then(() =>{
          console.log('Successfully enabled Bluetooth');
        }).catch(e => console.error(e));
      break;

    case 4:
      this.diagnostic.setBluetoothState(false).then(() =>{
        console.log('Successfully Disable Bluetooth');
      }).catch(e => console.error(e));
    break;

    case 5:
    this.diagnostic.requestBluetoothAuthorization().then(() =>{
      console.log('Bluetooth authorization was requested');
    }).catch(e => console.error(e));
    break;

    default:

      break;
    }
    
  }

  WifiFunction(i)
  {
    switch (i)
    {
    case 1:

      this.diagnostic.isWifiEnabled()
      .then((response) => {
        console.log("Wifi is enabled " + (response ? "enabled" : "disabled"));
      }).catch(e => console.error(e));
       break;

    case 2:
        this.diagnostic.setWifiState(true).then(() =>{
          console.log('Successfully enabled wifi');
        }).catch(e => console.error(e));
      break;

    case 3:
      this.diagnostic.setBluetoothState(false).then(() =>{
        console.log('Successfully Disable wifi');
      }).catch(e => console.error(e));
    break;

    default:

      break;
    }
    
  }

  CameraFunctionModule(i)
  {
    switch (i)
    {
    case 1:

    this.diagnostic.isCameraPresent().then((response) => {
      console.log('Camera is' + (response ? 'present' : 'absent'));
    }).catch(e => console.error(e));

    case 2:
        this.diagnostic.isCameraAvailable().then((response) =>{
          console.log('Camera is' + (response ? 'available' : 'not available'));
        }).catch(e => console.error(e));
      break;

    case 3:
      this.diagnostic.isCameraAuthorized().then((response) =>{
        console.log('Application is' + (response ? 'Authorized' : 'denied')+'Acces to the camera');
      }).catch(e => console.error(e));
    break;

    case 3:
    this.diagnostic.getCameraAuthorizationStatus().then((response) =>{
      if ( response == this.diagnostic.permissionStatus.GRANTED)
      {
        console.log('camera use is autharized');

      }
    }).catch(e => console.error(e));
  break;

    default:

      break;
    }

  }

  NFCFunctionModule(i)
  {
    switch (i)
    {
    case 1:

    this.diagnostic.registerNFCStateChangeHandler(function(state)
  {
    if (state == this.diagnostic.NFCState.UNKNOWN){ } 
        else if (state == this.diagnostic.NFCState.POWERED_OFF) { }
        else if (state == this.diagnostic.NFCState.POWERED_ON) { }
        else if (state == this.diagnostic.NFCState.UNAUTHORIZED) { }
        else if (state == this.diagnostic.NFCState.UNSUPPORTED) { }

  });

    case 2:
        this.diagnostic.isNFCPresent().then((response) =>{
          console.log('NFC Hardware is' + (response ? 'present' : 'absent')+'Acces to the camera');
        }).catch(e => console.error(e));
      break;

    case 3:
      this.diagnostic.switchToNFCSettings();
    break;
    default:

      break;
    }

  }

  ExternalStorageModule(i)
  {
    switch (i)
    {
    case 1:

    this.diagnostic.isExternalStorageAuthorized().then((response) =>{
      console.log('NFC Hardware is' + (response ? 'present' : 'absent')+'Acces to the camera');
    }).catch(e => console.error(e));
  break;

    case 2:
        this.diagnostic.getExternalStorageAuthorizationStatus().then((state) =>{
          if ( state === this.diagnostic.permissionStatus.GRANTED)
          {

          }
        }).catch(e => console.error(e));
      break;

    case 3:
      this.diagnostic.requestExternalStorageAuthorization().then((state) => {
        console.log("Authorization request for external storage use was " + (status == this.diagnostic.permissionStatus.GRANTED ? "granted" : "denied"));
      });
    break;
    default:

      break;
    }

  }

  CalendarFunctionModule(i)
  {
    switch (i)
    {
    case 1:

    this.diagnostic.isCalendarAuthorized().then((response) =>{
      console.log("App is " + (response ? "authorized" : "denied") + " access to calendar");
    }).catch(e => console.error(e));
  break;

    case 2:
        this.diagnostic.getCalendarAuthorizationStatus().then((state) =>{
          if ( state === this.diagnostic.permissionStatus.GRANTED)
          {
            console.log("Calendar use is authorized");
          }
        }).catch(e => console.error(e));
      break;

    case 3:
      this.diagnostic.requestCalendarAuthorization().then((state) => {
        if(status === this.diagnostic.permissionStatus.GRANTED){
          console.log("Calendar use is authorized");
      }
   }).catch(e => console.error(e));
    break;
    default:

      break;
    }

  }

  LocationFunctionModule(i)
  {
    switch (i)
    {
    case 1:

    this.diagnostic.getBluetoothState()
    .then((state) => {

      if (state == this.diagnostic.locationMode.HIGH_ACCURACY){ } 
      else if (state == this.diagnostic.locationMode.BATTERY_SAVING) { }
      else if (state == this.diagnostic.locationMode.DEVICE_ONLY) { }
      else if (state == this.diagnostic.locationMode.LOCATION_OFF) { }
      else
      {

      }

    }).catch(e => console.error(e));
    break;

    case 2:
        this.diagnostic.isLocationAvailable().then((response) =>{
          console.log("Location is " + (response ? "available" : "not available"));
        }).catch(e => console.error(e));
      break;

    case 3:
      this.diagnostic.isLocationEnabled().then((response) => {
        console.log("Location setting is" + (response ? "enabled" : "disabled"));
   }).catch(e => console.error(e));
    break;

    case 4:
      this.diagnostic.isGpsLocationAvailable().then((response) => {
        console.log("GPS location is " + (response ? "available" : "not available"));
      }).catch(e => console.error(e));
    break;

    case 5:
    this.diagnostic.isGpsLocationEnabled().then((response) => {
      console.log("GPS location is " + (response ? "enabled" : "disabled"));
    }).catch(e => console.error(e));
  break;

    default:

      break;
    }

  }








}
