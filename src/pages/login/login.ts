import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirstPage } from '../first/first';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  @ViewChild('username') uname;
  @ViewChild('password') password;
  constructor(public navCtrl: NavController, public navParams: NavParams, private fire: AngularFireAuth, public alertCtrl: AlertController ) {
  }

  alert(message: string)
  {
    {
      let alert = this.alertCtrl.create({
        title: 'Info',
        subTitle: message,
        buttons: ['OK']
      });
      alert.present();
    }  
  }

  signInuser()
  {
    this.fire.auth.signInWithEmailAndPassword(this.uname.value, this.password.value)
    .then( data => {
      console.log('got data', this.fire.auth.currentUser);
      this.alert('Succes! Your logged in as');
      this.navCtrl.setRoot(FirstPage);
    })
    .catch( error => {
      this.alert(error.message);
    });
    // console.log('Would sign in with', this.uname.value, this.password.value);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
