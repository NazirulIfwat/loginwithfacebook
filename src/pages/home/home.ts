import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirstPage } from '../first/first';
import { LoginPage } from '../login/login';
import { RegisterPage } from '../register/register';
import { FacebookPage } from '../facebook/facebook';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  signIn()
  {
  // this.navCtrl.push(FirstPage);
  // console.log(this.uname.value, this.password.value);
  this.navCtrl.push(LoginPage);
  }

  register()
  {
    this.navCtrl.push(RegisterPage);
  }

  fb()
  {
    this.navCtrl.push(FacebookPage);
  }
}

